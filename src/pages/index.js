/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import Layout from "../components/layout"
import Header from "../components/header"
import Logo from "../components/Logo"
import FooterBanner from "../components/footerBanner"


function Home(){
  return (
    <>
      <div>
        <div className="home">
          <Logo/>
          <div style={{position:'absolute',zIndex:-1}}>
            <FooterBanner/>
          </div>
        </div>
        {/*<div className="home" style={{backgroundColor:'black'}}>
          <FooterBanner/>
        </div>*/}
      </div>
    </>
  )
}


function LogoSvg(){
  return(
    <svg xmlns="http://www.w3.org/2000/svg" width={580} height={400}>
    <g>
      <title>background</title>
      <rect
        fill="#fff"
        id="canvas_background"
        height={402}
        width={582}
        y={-1}
        x={-1}
      />
      <g
        display="none"
        overflow="visible"
        y={0}
        x={0}
        height="100%"
        width="100%"
        id="canvasGrid"
      >
        <rect
          fill="url(#gridpattern)"
          strokeWidth={1}
          y={0}
          x={0}
          height="100%"
          width="100%"
        />
      </g>
    </g>
    <g>
      <text
        xmlSpace="preserve"
        textAnchor="start"
        fontFamily="Helvetica, Arial, sans-serif"
        fontSize={63}
        id="svg_1"
        y="214.453125"
        x="144.5"
        stroke="#000"
        fill="#000000"
      >
        PURE
      </text>
    </g>
  </svg>

  )
}

export default Home
