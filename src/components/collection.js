import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import styled from "styled-components"

const CollectionWrapper = styled.div`
  display:flex;
  flex-flow:row wrap;
`
const Image = styled.img`
  flex-grow:1;
  object-fit:contain;
  margin:0;
  width:300px;
`

export default function Collection(){
    let collectionName = 'collection1';
    const data = useStaticQuery(graphql`
    query {
       allFile{
        nodes{
            relativeDirectory
            relativePath
            id
            childImageSharp {
              fluid{
                src
                srcSet
                originalImg
                originalName
                aspectRatio
                sizes
              }
            }
        }
       }
    }
  `)
    
    let filtered = data.allFile.nodes.filter(d=>d.relativeDirectory===collectionName);
    return (
        <CollectionWrapper>
            {filtered.map(img=>{
                return(
                    <Image src={img.childImageSharp.fluid.src}/>
                )
            })}
        </CollectionWrapper>
    )
}
