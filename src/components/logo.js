import React from "react"
import { useStaticQuery, graphql } from "gatsby"


export default function Logo(){
    const data = useStaticQuery(graphql`
      query {
        file(relativePath: {eq: "pure-logo.png"}) {
          childImageSharp {
            fluid {
              src
            }
          }
        }
      }
    `
    )

    return(
      <img src={data.file.childImageSharp.fluid.src} style={{mixBlendMode:'multiply'}}/>
    )
  }