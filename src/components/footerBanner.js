import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import bottom from '../images/bottomv2.jpg' // relative path to image 


export default function FooterBanner(){
    const data = useStaticQuery(graphql`
      query {
        file(relativePath: {eq: "bottomv2.jpg"}) {
          childImageSharp {
            fluid(quality: 100) {
              src
            }
          }
        }
      }
    `
    )

    return(
      <img src={bottom}/>
    )
  }