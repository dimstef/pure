import { Link } from "gatsby"
import { navigate } from "gatsby"
import PropTypes from "prop-types"
import styled from "styled-components"
import {CSSTransition} from "react-transition-group";
import {useMediaQuery} from 'react-responsive';
import React, {useState,useEffect,useRef} from "react"
import { createRipples } from 'react-ripples'
import "./header.css"


const Ripple = createRipples({
  color: '#00000012',
  during: 800,
})

const HeaderWrapper = styled.div`
  height:60px;
  box-shadow: 0px 0px 9px -1px rgba(0, 0, 0, 0.2);
  display:flex;
  justify-content:center;
  align-items:center;
  position:fixed;
  animation:${props => props.isDesktopOrLaptop ? "header-from-top 1s" : "header-from-bottom 1s"};
  animation-fill-mode:forwards;
  width:100%;
  background-color:white;
`

const TextWrapper = styled.div`
  height:100%;
  width:100%;
  display:flex;
  flex-flow:column;
  justify-content:center;
  align-items:center;
`

const DrawerWrapper = styled.div`
  display:flex;
  justify-content:center;
  position:fixed;
  width:100%;
  left:0;
`
const StyledDrawer = styled.div`
  display: flex;
  flex-flow: column;
  justify-content: space-evenly;
  align-items: center;
  box-shadow: 0 0px 3px rgba(0,0,0,0.25), 
              0 2px 2px rgba(0,0,0,0.20), 
              0 4px 4px rgba(0,0,0,0.15), 
              0 8px 8px rgba(0,0,0,0.10),
              0 16px 16px rgba(0,0,0,0.05);
  height:100px;
  width:200px;
  background-color:white;
  border-radius:${props => props.isDesktopOrLaptop ? "10px 10px 25px 25px" : "25px 25px 10px 10px"};
  overflow:hidden;
`

const StyledPLink = styled.p`
  cursor:pointer;
  height:100%;
  width:100%;
  display:flex;
  justify-content:center;
  align-items:center;
  font-family: "Segoe UI",Arial,sans-serif;
  font-weight:500;
  margin:0;

`
const Navigation = styled.div`
  max-width:1200px;
  height:100%;
  display:flex;
  justify-content:center;
`
const Button = styled.button`
  flex-grow:1;
  cursor:pointer;
  background-color:transparent;
  border:0;
  font-family:-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen,
    Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif;
  font-weight:100;
  font-size:1.3rem;
  -webkit-tap-highlight-color: transparent;
  outline: none;
  transition:background-color 0.2s ease-in;
  :hover {
    background-color:#f5f5f5;
  }
`
const Header = ({ siteTitle }) => (
  <header
    style={{
      background: `rebeccapurple`,
      marginBottom: `1.45rem`,
    }}
  >
    <div
      style={{
        margin: `0 auto`,
        maxWidth: 960,
        padding: `1.45rem 1.0875rem`,
      }}
    >
      <h1 style={{ margin: 0 }}>
        <Link
          to="/"
          style={{
            color: `white`,
            textDecoration: `none`,
          }}
        >
          {siteTitle}
        </Link>
      </h1>
    </div>
  </header>
)

function useOutsideAlerter(ref,action) {
  /**
   * Alert if clicked on outside of element
   */
  function handleClickOutside(event) {
    if (ref.current && !ref.current.contains(event.target)) {
      action();
    }
  }

  useEffect(() => {
    // Bind the event listener
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      // Unbind the event listener on clean up
      document.removeEventListener("mousedown", handleClickOutside);
    };
  });
}


function PureHeader(){
  const headerRef = useRef(null);
  

  const [drawerOpen,setDrawerOpen] = useState(false);
  const isDesktopOrLaptop = useMediaQuery(
    { minDeviceWidth: 1224 },
  )
  const [options,setOptions] = useState([]);

  function closeDrawer(){
    setDrawerOpen(false);
  }

  useOutsideAlerter(headerRef,closeDrawer);
  function collectionOptions(){
    function toHome(){
      navigate('/');
    }
    
    function toCollection(){
      navigate('/collection1');
    }

    let options = [
      {
        label:'collection',
        action:toCollection
      },
    ]

    // Add home navigation when user is not at home
    if(window.location.pathname!=='/'){
      options = [...options,{
        label:'home',
        action:toHome
      }]
    }

    setOptions(options)
    setDrawerOpen(!drawerOpen);
  }

  return(
    <HeaderWrapper ref={headerRef} isDesktopOrLaptop={isDesktopOrLaptop}>
      <TextWrapper >
        <Drawer drawerOpen={drawerOpen} setDrawerOpen={setDrawerOpen} options={options}/>
        <Navigation className="navigation">
          <Button>Press</Button>
          <Button onClick={collectionOptions}>{window.location.pathname==='/'?'Clothes':'Explore'}</Button>
          <Button>About</Button>
        </Navigation>
      </TextWrapper>
    </HeaderWrapper>
  )
}

function Drawer({drawerOpen,setDrawerOpen,options}){

  const isDesktopOrLaptop = useMediaQuery(
    { minDeviceWidth: 1224 },
  )

  return(
    <CSSTransition classNames={isDesktopOrLaptop?'drawer-ds':'drawer'} 
    in={drawerOpen} 
    timeout={400}
    unmountOnExit>
      <DrawerWrapper>
        <StyledDrawer isDesktopOrLaptop={isDesktopOrLaptop}>
          {options.map(o=>{
            return <Ripple><StyledPLink onClick={()=>{
              setDrawerOpen(false);
              o.action()
            }}>{o.label}</StyledPLink></Ripple>
          })}
        </StyledDrawer>
      </DrawerWrapper>
      
    </CSSTransition>
  )
}

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}


export default PureHeader